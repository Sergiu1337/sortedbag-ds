#include "SortedBag.h"
#include "SortedBagIterator.h"
#include<exception>;

#include<iostream>
using namespace std;

SortedBag::SortedBag(Relation r) {
	//TODO - Implementation
	this->r = r;
	this->capacity = 1;
	this->currentNr = 0;
	this->elements = new TComp[this->capacity];
}

void SortedBag::add(TComp e) {
	//TODO - Implementation
	if (this->capacity == this->currentNr) {		// If the bag is full, we dinamically increase its size
		TComp* new_elements = new TComp[this->capacity * 2];	// Create new Array on the heap

		for (int i = 0; i < this->currentNr; i++) {	// Go through all elements of the current bag
			new_elements[i] = this->elements[i];	// Add every element to the new array
		}

		delete[] this->elements;			// Delete old array
		this->elements = new_elements;		// Make it the new one
		this->capacity = this->capacity * 2;				// Let the class know we can now hold more items
	}

	this->elements[this->currentNr++] = e;  // Add element at the top and increase currentNr

	
	/*																	// This is faster but I did not manage to get it working correctly
	int i, j;
	TComp* key;
	for (int i = 1; i < this->currentNr; i++) {
		key = &this->elements[i];
		j = i - 1;
		while (j >= 0 && this->r(this->elements[j], *key) == false) {
			this->elements[j + 1] = this->elements[j];
			j = j - 1;
		}
		this->elements[j + 1] = *key;
	}
	*/
	
	// After we add the element (and dinamically increase the size if need be), we sort the bag (since it is a sorted bag);
	for (int i = 0; i < this->currentNr - 1; i++) {	// Iterate through all elements of the array almost 2 times (n^2)
		for (int j = i + 1; j < this->currentNr; j++) {
			if (this->r(this->elements[i], this->elements[j]) == false) {// If the given condition between the given element and the current element is true
				TComp aux = this->elements[i];					// Switch between the elements
				this->elements[i] = this->elements[j];
				this->elements[j] = aux;
			}
		}
	}
}
// Best case : Omega(n) // When we do NOT have to increase array size, we just add the element, and we sort the array using an optimized bubble sort
// Worst case : O(n) - creating a new array + O(n^2) for the sorting
// Average case : I believe Theta(n^2) for the sorting


bool SortedBag::remove(TComp e) {
	
	if (this->currentNr < this->capacity / 2) {		// If we have less than half the capacity of elements, make the new array smaller;
		int new_capacity = this->capacity / 2 + 1;		// Modify capacity
		TComp* new_elements = new TComp[new_capacity];	// Create new array with a smaller allocated memory
		this->capacity = new_capacity;					// Change the capacity that the class knows it has
		for (int i = 0; i < this->currentNr; i++)		// Go through all elements in the array and copy them in the new array;
			new_elements[i] = this->elements[i];
		delete[] this->elements;						// Free the previously allocated memory
		this->elements = new_elements;					// Give the new memory to the class
	}


	for (int i = this->currentNr - 1; i >= 0; i--) {
		if (this->elements[i] == e) {
			for (int j = i; j < this->currentNr - 1; j++) {	// Move all items one position down while overwriting the element we need to remove
				this->elements[j] = this->elements[j + 1];
			}
			this->elements[currentNr] = 0;		// Reset last element (redundant because we decrease currentNr anyway so it is technically not accessible)
			this->currentNr--;					// Decrease currentNr because we removed one element
			return true;		// Let the user know we removed one element
		}
	}
	return false;				// Let the user know we didn't remove anything
}
// Best case : Omega(n) - we only go through the array once because we remove the last element so we do not need to shift all positions AND we do not need to decrease the array size
// Worst case : O ((n^2) + n) - we need to shift all positions AND create a new array
// Average case : Theta(n^2)


bool SortedBag::search(TComp elem) const {
	//TODO - Implementation

	for (int i = 0; i < this->currentNr; i++) {
		if (this->elements[i] == elem) {
			return true;
		}
	}
	return false;
}
// Best case : Omega(1) - we find it on the first position
// Worst case : O(n) - we find it on the last position
// Average case : Theta(log n) - we might find it sooner


int SortedBag::nrOccurrences(TComp elem) const {
	//TODO - Implementation

	/*													// For debugging purposes
	for (int i = 0; i < this->currentNr; i++) {
		cout << this->elements[i] << " ";
	}
	*/

	int counter = 0;

	for (int i = 0; i < this->currentNr; i++) {
		if (this->elements[i] == elem) {
			counter++;
		}
	}
	return counter;
}
// Theta(n) - we go through it all the time



int SortedBag::size() const {
	//TODO - Implementation

	return this->currentNr;
}
// Theta(1)


bool SortedBag::isEmpty() const {
	//TODO - Implementation

	if (this->currentNr == 0) {
		return true;
	}
	else {
		return false;
	}
}
void SortedBag::addOccurrences(int nr, TComp elem)	// THIS WAS THE BONUS FUNCTION
{
	int counter = nrOccurrences(elem);
	if (counter > 0) {
		if (nr < 0)
			throw exception();
		else
			for (int i = 0; i < nr; i++) {
				this->add(elem);
			}
	}
	else {
		if (nr < 0)
			throw exception();
		else {
			this->add(elem);
		}
	}
}
// Theta(n)


SortedBagIterator SortedBag::iterator() const {
	return SortedBagIterator(*this);
}


SortedBag::~SortedBag() {
	//TODO - Implementation
	delete[] this->elements;
}
// Theta(1)
