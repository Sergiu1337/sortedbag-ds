#include "SortedBagIterator.h"
#include "SortedBag.h"
#include <exception>

using namespace std;

SortedBagIterator::SortedBagIterator(const SortedBag& b) : bag(b) {
	//TODO - Implementation

	this->current = 0;
}

TComp SortedBagIterator::getCurrent() {
	//TODO - Implementation
	if (this->current < bag.size()) {
		return bag.elements[this->current];
	}
	else
		throw exception();
}

bool SortedBagIterator::valid() {
	//TODO - Implementation

	if (this->current < bag.size()) {
		return true;
	}
	else {
		return false;
	}
}

void SortedBagIterator::next() {
	//TODO - Implementation
	if (this->current < bag.size()) {
		this->current+=1;
	}
	else
		throw exception();
}

void SortedBagIterator::first() {
	//TODO - Implementation

	this->current = 0;
}

