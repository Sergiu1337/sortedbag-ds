#include "SortedBag.h"
#include "SortedBagIterator.h"
#include <iostream>
#include "ShortTest.h"
#include "ExtendedTest.h"

using namespace std;

int main() {
	testAll();			// Everything works
	testAllExtended();	// Everything  works but it takes a lot lot of time
	
	cout << "Test over" << endl;
	system("pause");
}